let posts = []
posts.first = function () {
  return this[0]
}
posts.last = function () {
  return this[this.length - 1]
}
let winlengthcheck = 0

function createDisks(disknum){
  for(let i = 0; i < disknum; i++){
    let ids = "disk" + i
    let disk ={
      html : document.createElement("div"),
      id : ids,
      attributes : ["draggable", "true", "ondragstart", "drag(event)", "class", "disk"],
      width : 50 + i * 20,
      position: i
    }
    disk.html.id = disk.id
    disk.html.style.width = disk.width + "px"
    disk.html.style.height = "20px"
    attrsetter(disk.html, disk.attributes)
    posts.first().html.appendChild(disk.html)
    posts[0].disks.push(disk)
  }
}

function createPosts(postnum){
    for(let i = 0; i < postnum; i++){
      let post = {
        html : document.createElement("div"),
        id : "post" + i,
        attributes : ["ondrop", "drop(event)", "ondragover", "allowDrop(event)", "class", "pole"],
        height : 200,
        disks : []
      }
      post.html.style.height = post.height + "px"
      post.html.id = post.id
      attrsetter(post.html, post.attributes)
      posts.push(post)
      document.body.appendChild(post.html)
    }
}

function attrsetter(html, attributes){
  if(attributes.length%2 != 0){
    console.log("Array of attributes not even, How did this happen?")
  } else{
    for(let i = 0; i < attributes.length; i+=2){
      html.setAttribute(attributes[i], attributes[i+1])
    }
  }
}

function updatePostHeight(){
  let postheight = (50 + posts[0].disks.length * 26) + "px"
  for(let x of posts){
    x.html.style.height = postheight
  }
}

function startGame(disknum = 4, postnum = 3){
  createPosts(postnum)
  createDisks(disknum)
  updatePostHeight()
  winlengthcheck = disknum
}

function gameLogic(disk,evid){
  for(let post of posts){
    var check
    if(typeof post.disks[0] != "undefined"){
      var postcheck = post.disks[0].id[post.disks[0].id.length - 1]
      var diskcheck = disk.id[disk.id.length -1]
      check = Boolean(diskcheck < postcheck)
    } else if(check != true){
      check = false
    }
    if(post.id === evid && check){
      post.disks.splice(0,0, disk)
      return true
    }
  }
}

function diskMoveCheck(diskID, postID){
  var postAdd, postRemove, diskCheck, disk
  for(let i = 0; i < posts.length; i++){
    //Finds the post Object based on even ID
    if(posts[i].id == postID){
      postAdd = posts[i]
      if(typeof postAdd.disks[0] != "undefined"){
        diskCheck = postAdd.disks[0].id
      } else {
        diskCheck = true
      }
    }

    //Finds the post that disk is on
    for(let disksofpost of posts[i].disks){
      if(disksofpost.id == diskID){
        disk = disksofpost
        postRemove = posts[i]
      }
    }
    if(typeof postRemove != "undefined"){
      if(postRemove.disks[0] != disk){
        return false
      }
    }
  }

  let lastcharofdiskcheckid = diskCheck[diskCheck.length - 1]
  let lastcharofdiskid = disk.id[disk.id.length - 1]
  if(lastcharofdiskcheckid > lastcharofdiskid || diskCheck == true){
    postRemove.disks.splice(0,1)
    postAdd.disks.splice(0,0,disk)
    return true
  } else {
    return false
  }
}

function winCheck(){
  if(posts[2].disks.length == winlengthcheck){
      let text = document.createTextNode("Congratz you have won Tower of Hanoi for " + winlengthcheck + " disks on the tower")
      document.body.appendChild(text);
  }
}

function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text/plain", ev.target.id);
}

function drop(ev) {
  let diskID = ev.dataTransfer.getData("text")
  let postID = ev.target.id
  if(diskMoveCheck(diskID, postID)){
    ev.preventDefault();
    ev.target.prepend(document.getElementById(diskID));
    winCheck()
  }
}

let gamestartnum, imput;
function booton(){
  gamestartnum = document.getElementById("disknum").value
  if(gamestartnum == undefined){
    startGame()
    console.log("error invalid number")
  } else {
    startGame(gamestartnum)
  }
}